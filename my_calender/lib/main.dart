import 'package:flutter/material.dart';
import 'dart:async';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home:RootView(),
    );
  }
}

class RootView extends StatefulWidget {
  @override
  RootViewState createState() => RootViewState();  
}

class RootViewState extends State<RootView>{
  var cards = <Widget>[];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.library_add),
            onPressed: _openCardView,
          )
        ],
      ),
      body: Text(cards.length.toString()),
    );
  }

  void _openCardView(){
    Navigator.of(context).push(MaterialPageRoute<void>(builder: (_) {
      return Scaffold(
        appBar: AppBar(),
        body: CardView(
          cards: cards,
        ),
      );
    }));
  }
}

class AddCardViewState extends State<AddCardView>{

  AddCardViewState(
    this.cards,
  );

  List<Widget> cards;
  var name = "";
  var description = "";
  var kind = "Fixed Duration";
  var duration = 1;

  Widget build(BuildContext context){
    var children = <Widget>[
      TextField(
        decoration: InputDecoration( border: UnderlineInputBorder(), labelText: 'Name', contentPadding: EdgeInsets.all(0) ),
        onSubmitted: (newName){
          this.name = newName;
        },
      ),
      SizedBox( height: 10,),
      TextField(
        decoration: InputDecoration( border: UnderlineInputBorder(), labelText: 'Description', contentPadding: EdgeInsets.all(0) ),
        onSubmitted: (newDescription){
          this.description = newDescription;
        },
      ),
      SizedBox( height: 10,),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text("Card Type:"),
          DropdownButton<String>(
            value: this.kind,
            items: <String>["Fixed Duration"]
              .map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(value.toString()),
                );
              })
              .toList(),
            onChanged: (String value){
              setState((){
                this.kind = value;
              });
            },
          ),
        ],
      ),
    ];

    if(this.kind == "Fixed Duration"){
      children.add(Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text("Duration:"),
          DropdownButton<int>(
            value: this.duration,
            items: <int>[1, 2, 3, 4]
              .map<DropdownMenuItem<int>>((int value) {
                return DropdownMenuItem<int>(
                  value: value,
                  child: Text(value.toString()),
                );
              })
              .toList(),
            onChanged: (int value){
              setState((){
                this.duration = value;
              });
            },
          ),
        ],
      ));
    }

    children.add(FlatButton(
      child: Center( child:Text("Save Card")),
      onPressed: (){
        this.cards.add(Card(child: Text(this.name),));
      },
    ));
    return Container( padding: EdgeInsets.all(10), child:Card(
      elevation: 10,
      child: Container( padding: EdgeInsets.all(10), child:ListView(
        /* mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start, */
        children: children,
        shrinkWrap: true,
      ),
    )));
  }

}

class AddCardView extends StatefulWidget{

  AddCardView(
    this.cards,
  );

  final List<Widget> cards;

  AddCardViewState createState () => AddCardViewState(this.cards);
}

class CardViewState extends State<CardView>{

  CardViewState(
    this.cards,
  );

  List<Widget> cards;

  @override
  Widget build(BuildContext context){
    var children = cards.sublist(0);
    children.add(ListTile(
      title: Text('Add New Card'),
      trailing: Icon(Icons.add),
      onTap: addCard,
    ));
    return ListView(children: children);
  }

  void addCard(){
    Navigator.of(context).push(MaterialPageRoute<void>(builder: (_){
      return Scaffold(
        appBar: AppBar(),
        body: AddCardView(cards),
      );
    }));
  }
}

class CardView extends StatefulWidget {

  const CardView({
    this.cards,
  });

  @override
  State<StatefulWidget> createState() {
    return CardViewState(this.cards);
  }

  final List<Widget> cards;
}

class DayView extends StatefulWidget {
  @override
  _DayViewState createState() {
    return new _DayViewState();
  }
}

class _DayViewState extends State<DayView>{
  TimeOfDay timeOfDay;

  void initState(){
    super.initState();
    var interval = Duration(seconds: 1);
    timeOfDay = TimeOfDay.now();
    new Timer.periodic(interval, (Timer t) => (this.setState(() => this.timeOfDay = TimeOfDay.now())));
  }

  @override
  Widget build(BuildContext context) {
    var hours = <Widget>[];
    for (int i = 0; i <= 24; i++) {
      hours.add(Row(
        children: <Widget>[
          Text(i.toString()),
          Flexible(
            child: Container(
              color: Colors.black,
              height: 1,
            )
          )
        ],
      ));
      hours.add(Container(
        height: 100,
      ));
    }
    hours[this.timeOfDay.hour*2+1] = Stack(
      overflow: Overflow.visible,
      children: <Widget>[
        hours[this.timeOfDay.hour*2+1],
        Positioned(
          left: 0,
          right: 0,
          top: -7 + 117*this.timeOfDay.minute/60,
          height: 5,
          child: Container(
            color: Colors.blueAccent.withAlpha(175),
          ),
        )
      ],
    );
    return Stack(
      children: <Widget>[
        Container(
          color: Colors.black12,
          padding: EdgeInsets.all(10),
          child: ListView(
            children: hours,
          )
        )
      ]
    );
  }
}